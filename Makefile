VERSION ?= 0.1.0
NAME ?= hermantolim/okteto-arch-rust
TZ ?= UTC
BUILD_ARG ?= --build-arg TZ=$(TZ)

.PHONY: all build dkr tag dkr_tag

all: build tag

build:
	okteto build --tag $(NAME):$(VERSION) ./src

dkr:
	docker build --rm --network=host -t $(NAME):$(VERSION) $(BUILD_ARG) ./src

tag:
	okteto build --tag $(NAME):latest ./src

dkr_tag:
	docker tag $(NAME):$(VERSION) $(NAME):latest