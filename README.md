# Okteto Rust Development Setup

This is docker rust image based on [archlinux](https://archlinux.org) base image, prepared
for handling cargo package compilation backed by [okteto](https://okteto.com)
kubernetes cluster


## How To


### Sign-up to Okteto
Register an account at [cloud.okteto.com](https://cloud.okteto.com).


### Install Command Line Client
install `okteto` command line client.

```sh
curl -sSL get.okteto.com | sh
```


### Login with CLI client
login using `okteto` cli and get your kubeernetes config, and optionally
creating custom namespace for dev environment.

```sh
# login
okteto login

# create a namespace for dev (optional)
okteto create namespace dev-your_base_namespace

# downloading kubernetes config
okteto namespace
```


### Preparing Rust Workspace
clone this repository to your local workspace

```sh
# cloning this repo
git clone https://bitbucket.org/hermantolim/docker-okteto-arch-rust okteto-rust

# change your working directory
cd okteto-rust/app
```


### Initiate the Project

```sh
# create your rust project with cargo that point to the `okteto-rust/app` directory
cargo init --name my-project-name

# for existing project just copy contents your project dir to `okteto-rust/app` directory
cp -a /home/user/my-project/. .
```


### Turning up the Cluster

from the `okteto-rust/app` directory run

```sh
okteto up
```

now, enjoy your new kubernetes development environment.


### Shutting Down
to shutdown your dev environment simply run

```sh
okteto down -v
```

this will shutdown the dev and detach the persistent volume


## Structure

### Volume Mountpoint
By default the environment is configured to mount 3 volumes on:

- `/okteto` [`$HOME`, put your custom shell rcfiles here.]
- `/okteto/app` [local directory mountpoint `app/` synced with syncthing]
- `/rust` [rust root dir containing `$RUSTUP_HOME` and `$CARGO_HOME`]


### Source
- `app/` [rust project workdir]
  - `okteto.yml` [okteto dev environment manifest]
  - `.stignore` [synthing ignore list]
- `src/` [the archlinux + rust docker source]


## Tips
You can detach your okteto env using `exit` command and then
run `okteto exec` for running any shell script later. This is
useful when installing other cargo utilities.

```sh
okteto exec cargo install -j2 cargo-web wasm-pack
```

The image also pre-installed with `postgresql-libs`, `mariadb-libs`
and `sqlite` database libraries, to assist in installing `diesel-cli`,
just remove the `okteto exec` if you're installing within the container

```sh
okteto exec cargo install diesel-cli
```


All of your cargo installed binary will persist in the volume
including cache, library repository source, cargo registry,
rustup temporary file downloads, etc.

to reduce your storage space usage you may need to regulary
emptying these directories:

- `$CARGO_HOME/git`
- `$CARGO_HOME/registry`
- `$RUSTUP_HOME/tmp`
- `$RUSTUP_HOME/downloads`


### Copying Files to $HOME

By default okteto will use `synthing` to sync your local `app` directory
to the cloud dev container. You can put file like `.bashrc` or `.vimrc`
into your `app` directory then `mv` it to other directory from inside the container

```sh
# moving file synced to container into the $HOME mount volume
mv /okteto/app/.bashrc /okteto/.bashrc
```
