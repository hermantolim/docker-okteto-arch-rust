set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim

" Install Plugin
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'andymass/vim-matchup'
Plugin 'bling/vim-airline'
Plugin 'lilydjwg/colorizer'
Plugin 'tpope/vim-fugitive'
Plugin 'rust-lang/rust.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'gorkunov/smartpairs.vim'
Plugin 'scrooloose/syntastic'

" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on

" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal

syntax on

set undofile  
set undodir=/tmp
set encoding=utf-8
set ffs=unix,dos,mac

set backup
set backupdir=$HOME/temp/vim_backups/ 
set directory=$HOME/temp/vim_swp/ 
set noswapfile

set backspace=2
set matchtime=2
set formatoptions-=tc
set nosmartindent
set autoindent
set cindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set matchpairs+=<:>
set showmatch

set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

set clipboard=unnamed
set selection=exclusive
set number

autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
