#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ $DISPLAY ]] && shopt -s checkwinsize

#case ${TERM} in
#  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

#    ;;
#  screen*)
#    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
#    ;;
#esac

precmd() {
    e=$?
    (( $e )) && echo -n $e
}

alias ls='exa --color=auto'
alias la='ls -a'
alias ll='ls -al'
alias grep='grep --color=auto'
alias rg='rg --color=auto'
alias cg='cargo'
alias cgs='cargo ssearch --limit=100'
alias cgi='cargo init'
alias cgb='cargo build'
alias cgr='cargo build --release'
alias psu='pacman -Syu --noconfirm'
alias psn='pacman -S --needed --noconfirm'
alias ysu='yay -Syu --noconfirm'
alias ysn='yay -S --needed --noconfirm --noeditmenu'
alias rs='rustup'
alias rg='rg --color=auto'

shopt -sq autocd \
    cdable_vars \
    cdspell \
    dirspell \
    hostcomplete \
    histappend \
    progcomp_alias

BLACK=$'\E[38;5;0m'
RED=$'\E[38;5;1m'
GREEN=$'\E[38;5;2m'
YELLOW=$'\E[38;5;3m'
BLUE=$'\E[38;5;4m'
MAGENTA=$'\E[38;5;5m'
CYAN=$'\E[38;5;6m'
WHITE=$'\E[38;5;7m'
BOLD=$'\E[1m'
UNDERLINE=$'\E[4m'
RESET=$'\E[0m'
LIGHTBLUE=$'\E[38;5;44m'

HISTSIZE=10000
HISTTIMEFORMAT="%a %d %b %Y %T"
HISTFILE=~/.bash_history

PS1_OPEN=$'${BLUE}.--( ${RESET}'
PS1_BOPEN=$'${BLUE} )+[ ${RESET}'
PS1_BCLOSE=$'${BLUE} ]:< ${RESET}'
PS1_USER=$'${CYAN}\u${RESET}'
PS1_WDIR=$'${YELLOW}\w${RESET}'
PS1_CMD=$'${BLUE}\n\`= ${RED}$(precmd)${RESET} ${GREEN}\$${RESET} '
PS1=${PS1_OPEN}${PS1_USER}${PS1_BOPEN}${PS1_WDIR}${PS1_BCLOSE}${PS1_CMD}

export PROMPT_DIRTRIM=3
export CARGO_HOME=${CARGO_HOME:-/rust/.cargo}
export RUSTUP_HOME=${RUSTUP_HOME:-/rust/.rustup}
export RUSTFLAGS="-C link-arg=-s"
export RUST_LOG=info
export RUST_BACKTRACE=full
export PATH=/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:${HOME}/.local/bin:${CARGO_HOME}/bin:/var/okteto/bin

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

_try() {
    command -v $1 >/dev/null
}

_try rustup && source <(rustup completions bash)
_try helm && source <(helm completion bash)
_try kubectl && source <(kubectl completion bash)

if _try rustc; then
    CARGO_COMPL="$(rustc --print sysroot)/etc/bash_completion.d/cargo"
    [ -r $CARGO_COMPL ] && source $CARGO_COMPL
fi

if [[ ! -d ${RUSTUP_HOME} ]]; then
    rustup toolchain install nightly \
    && rustup default nightly \
    && rustup component add \
        clippy rustfmt \
        rls rust-analysis rust-src
fi